package qiql

import ()

type TypeVisitor struct {
	Context string
}

func (v *TypeVisitor) Print(expr Expr) string {
	expr.Accept(v)
	return v.Context
}

func (v *TypeVisitor) VisitUnary(unary *Unary) {
	v.Context = "unary"
}

func (v *TypeVisitor) VisitLiteral(literal *Literal) {
	v.Context = "literal"
}

func (v *TypeVisitor) VisitStrict(strict *Strict) {
	v.Context = "strict"
}

func (v *TypeVisitor) VisitBoolean(boolean *Boolean) {
	v.Context = "boolean"
}

func (v *TypeVisitor) VisitGrouping(grouping *Grouping) {
	v.Context = "grouping"
}

func (v *TypeVisitor) VisitAssignment(assignment *Assignment) {
	v.Context = "assignment"
}
