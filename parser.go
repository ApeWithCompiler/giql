package qiql

import (
	"fmt"
)

type ParseError struct {
	Token         Token
	StackPosition int
	Caller        string
	Message       string
}

type Parser struct {
	Tokens  []Token
	Current int
	Errors  []ParseError
}

func NewParser(tokens []Token) Parser {
	return Parser{
		Tokens:  tokens,
		Current: 0,
	}
}

func (parser *Parser) ClearErrors() {
	parser.Errors = nil
}

func (parser *Parser) Parse() []Expr {
	var exprs []Expr

	exprs = append(exprs, parser.Expression())

	start_position := parser.Current
	for parser.peek().Type != EOF {
		exprs = append(exprs, parser.Expression())

		// Expression() made no change in position
		// Loop would be stuck
		if start_position == parser.Current {
			parser.error("parse", "Infinite loop detected.")
			break
		}
	}
	return exprs
}

func (parser *Parser) Expression() Expr {
	return parser.conjunction()
}

func (parser *Parser) conjunction() Expr {
	var expr Expr
	var operator Token
	var right Expr

	expr = parser.disjunction()

	for parser.match(OR) == true {
		operator = parser.previous()
		right = parser.disjunction()
		expr = &Boolean{Left: expr, Operator: operator, Right: right}
	}
	return expr
}

func (parser *Parser) disjunction() Expr {
	var expr Expr
	var operator Token
	var right Expr

	expr = parser.unary()

	for parser.match(AND) == true {
		operator = parser.previous()
		right = parser.unary()
		expr = &Boolean{Left: expr, Operator: operator, Right: right}
	}
	return expr
}

func (parser *Parser) unary() Expr {
	if parser.match(NOT, MINUS, PLUS, POUND, TILDE, DOLAR) {
		operator := parser.previous()
		right := parser.unary()
		return &Unary{Operator: operator, Right: right}
	}

	return parser.primary()
}

func (parser *Parser) primary() Expr {
	if parser.match(NUMBER, STRING) {
		return &Literal{Value: parser.previous().Lexeme}
	}

	if parser.match(STRICT) {
		return &Strict{Value: parser.previous().Lexeme}
	}

	if parser.match(LEFT_PAREN) {
		expr := parser.Expression()
		parser.consume(RIGHT_PAREN, "Expect ')' after expression.")
		return &Grouping{Expr: expr}
	}

	if parser.match(IDENTIFIER) {
		identifier := &Literal{Value: parser.previous().Lexeme}
		parser.consume(COLON, "Expect ':' after identifier.")
		parser.consume(STRING, "Expect Value for assignement.")
		value := &Literal{Value: parser.previous().Lexeme}
		return &Assignment{Identifier: identifier, Value: value}
	}

	parser.error("literal", "Unidentifiable token")
	return &Literal{Value: "ERR"}
}

func (parser *Parser) consume(ttype TokenType, err string) Token {
	if parser.check(ttype) {
		return parser.advance()
	}

	parser.error("consume", err)
	return Token{Type: ERR, Lexeme: "ERR"}
}

func (parser *Parser) HasErrors() bool {
	return len(parser.Errors) > 0
}

func (parser *Parser) error(caller, msg string) {
	err := ParseError{
		Token:         parser.getToken(parser.Current),
		StackPosition: parser.Current,
		Caller:        caller,
		Message:       msg,
	}

	parser.Errors = append(parser.Errors, err)
}

func (parser *Parser) match(types ...TokenType) bool {
	for _, t := range types {
		if parser.check(t) {
			parser.advance()
			return true
		}
	}

	return false
}

func (parser *Parser) check(ttype TokenType) bool {
	if parser.isAtEnd() {
		return false
	}
	return (parser.peek().Type == ttype)
}

func (parser *Parser) advance() Token {
	if !parser.isAtEnd() {
		parser.Current += 1
	}
	return parser.previous()
}

func (parser *Parser) isAtEnd() bool {
	return parser.peek().Type == EOF
}

func (parser *Parser) peek() Token {
	return parser.getToken(parser.Current)
}

func (parser *Parser) previous() Token {
	return parser.getToken(parser.Current - 1)
}

func (parser *Parser) getToken(index int) Token {
	if index < 0 {
		msg := "Parser requested token index below 0"
		parser.error("getToken", msg)
		return NewToken(ERR, msg)
	}
	if index > len(parser.Tokens) {
		msg := fmt.Sprintf("Parser requested index %v but only has %v tokens", index, len(parser.Tokens))
		parser.error("getToken", msg)
		return NewToken(ERR, msg)
	}

	return parser.Tokens[index]
}

type FallbackParser struct {
	Tokens []Token
}

func NewFallbackParser(tokens []Token) FallbackParser {
	return FallbackParser{
		Tokens: tokens,
	}
}

func (parser *FallbackParser) Parse() []Expr {
	var exprs []Expr

	for _, t := range parser.Tokens {
		if parser.isLiteral(t) {
			l := &Literal{Value: t.Lexeme}
			exprs = append(exprs, l)
		}
	}

	return exprs
}

func (parser *FallbackParser) isLiteral(token Token) bool {
	if token.Type == NUMBER || token.Type == STRING {
		return true
	}

	return false
}
