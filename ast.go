package qiql

import ()

type Expr interface {
	Accept(visitor Visitor)
}

/*
*
* Unary
*
 */
type Unary struct {
	Operator Token
	Right    Expr
}

func (unary *Unary) Accept(visitor Visitor) {
	visitor.VisitUnary(unary)
}

/*
*
* Literal
*
 */
type Literal struct {
	Value string
}

func (literal *Literal) Accept(visitor Visitor) {
	visitor.VisitLiteral(literal)
}

/*
*
* Strict
*
 */
type Strict struct {
	Value string
}

func (strict *Strict) Accept(visitor Visitor) {
	visitor.VisitStrict(strict)
}

/*
*
* Boolean
*
 */
type Boolean struct {
	Left     Expr
	Operator Token
	Right    Expr
}

func (boolean *Boolean) Accept(visitor Visitor) {
	visitor.VisitBoolean(boolean)
}

/*
*
* Grouping
*
 */
type Grouping struct {
	Expr Expr
}

func (grouping *Grouping) Accept(visitor Visitor) {
	visitor.VisitGrouping(grouping)
}

/*
*
* Assignement
*
 */
type Assignment struct {
	Identifier Expr
	Value      Expr
}

func (assignment *Assignment) Accept(visitor Visitor) {
	visitor.VisitAssignment(assignment)
}
