package qiql

import (
	"fmt"
)

type ParserDebugger struct {
}

func (dbg *ParserDebugger) PrintStack(parser Parser, traceName string) {
	fmt.Printf("========= [%v] Parser Stack =========\n", traceName)
	for i, token := range parser.Tokens {
		if i != parser.Current {
			fmt.Printf("%v: Token{Type: %v, Lexeme: %v}\n", i, token.Type, token.Lexeme)
		} else {
			fmt.Printf("%v: Token{Type: %v, Lexeme: %v} <=== Current\n", i, token.Type, token.Lexeme)
		}
	}
}
