package qiql

import ()

/*
*
* Structures with JSON annotations
* representing the AST
*
 */

type JsonAstNode struct {
	Type       string      `json:"type"`
	Attributes interface{} `json:"attributes"`
}

type JsonAstUnary struct {
	Operator interface{} `json:"operator"`
	Right    interface{} `json:"right"`
}

type JsonAstLiteral struct {
	Value string `json:"value"`
}

type JsonAstStrict struct {
	Value string `json:"value"`
}

type JsonAstBoolean struct {
	Left     interface{} `json:"left"`
	Operator interface{} `json:"operator"`
	Right    interface{} `json:"right"`
}

type JsonAstGrouping struct {
	Expr interface{} `json:"expr"`
}

type JsonAstAssignment struct {
	Identifier interface{} `json:"identifier"`
	Value      interface{} `json:"value"`
}

/*
*
* Helper class, mitigating the constraint
* of an visitor interface without return type
* for recursive usage.
*
* StackItem is a container class for the items within the stack.
* If working with these items requires more data, it can be
* provided here.
 */
type StackItem struct {
	Id      int
	Content JsonAstNode
}

type Stack struct {
	idCounter int
	Items     []StackItem
}

func (s *Stack) Push(n JsonAstNode) {
	i := StackItem{Id: s.idCounter, Content: n}
	s.Items = append(s.Items, i)
	s.idCounter += 1
}

func (s *Stack) Pop() JsonAstNode {
	i := s.Items[len(s.Items)-1]
	s.Items = s.Items[:len(s.Items)-1]
	return i.Content
}

/*
*
* Serializer Visitor
*
 */

type JsonSerializerVisitor struct {
	Stack Stack
}

func (v *JsonSerializerVisitor) Parse(expr Expr) JsonAstNode {
	expr.Accept(v)
	return v.Stack.Pop()
}

func (v *JsonSerializerVisitor) VisitUnary(unary *Unary) {
	unary.Right.Accept(v)
	right := v.Stack.Pop()

	node := JsonAstNode{
		Type: "unary",
		Attributes: JsonAstUnary{
			Operator: unary.Operator.Lexeme,
			Right:    right,
		},
	}

	v.Stack.Push(node)
}

func (v *JsonSerializerVisitor) VisitLiteral(literal *Literal) {
	node := JsonAstNode{
		Type: "literal",
		Attributes: JsonAstLiteral{
			Value: literal.Value,
		},
	}
	v.Stack.Push(node)
}

func (v *JsonSerializerVisitor) VisitStrict(strict *Strict) {
	node := JsonAstNode{
		Type: "strict",
		Attributes: JsonAstStrict{
			Value: strict.Value,
		},
	}
	v.Stack.Push(node)
}

func (v *JsonSerializerVisitor) VisitGrouping(grouping *Grouping) {
	grouping.Expr.Accept(v)
	expr := v.Stack.Pop()

	node := JsonAstNode{
		Type: "grouping",
		Attributes: JsonAstGrouping{
			Expr: expr,
		},
	}

	v.Stack.Push(node)
}

func (v *JsonSerializerVisitor) VisitBoolean(boolean *Boolean) {
	boolean.Left.Accept(v)
	left := v.Stack.Pop()

	boolean.Right.Accept(v)
	right := v.Stack.Pop()

	node := JsonAstNode{
		Type: "boolean",
		Attributes: JsonAstBoolean{
			Left:     left,
			Operator: boolean.Operator.Lexeme,
			Right:    right,
		},
	}

	v.Stack.Push(node)
}

func (v *JsonSerializerVisitor) VisitAssignment(assignment *Assignment) {
	assignment.Identifier.Accept(v)
	identifier := v.Stack.Pop()

	assignment.Value.Accept(v)
	value := v.Stack.Pop()

	node := JsonAstNode{
		Type: "assignement",
		Attributes: JsonAstAssignment{
			Identifier: identifier,
			Value:      value,
		},
	}

	v.Stack.Push(node)
}
