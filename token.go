package qiql

import ()

type TokenType int

const (
	// Single-charakter tokens
	// Enum range 0-99
	COLON       TokenType = 0
	LEFT_PAREN            = 1
	RIGHT_PAREN           = 2
	PLUS                  = 3
	MINUS                 = 4
	STAR                  = 5
	TILDE                 = 6
	POUND                 = 7
	DOLAR                 = 8

	// One or two charakter tokens
	// Enum range 100-199

	// Literals
	// Enum range 200-299
	IDENTIFIER = 200
	STRING     = 201
	NUMBER     = 202
	STRICT     = 203

	// Keywords
	// Enum range 300-399
	NOT = 300
	AND = 301
	OR  = 302

	// End of file
	EOF = 400
	//Type for internal error
	ERR = 401
)

func KeywordMap() map[string]TokenType {
	m := make(map[string]TokenType)
	m["NOT"] = NOT
	m["AND"] = AND
	m["OR"] = OR

	return m
}

type Token struct {
	Type   TokenType
	Lexeme string
}

func NewToken(ttype TokenType, lexeme string) Token {
	return Token{
		Type:   ttype,
		Lexeme: lexeme,
	}
}
