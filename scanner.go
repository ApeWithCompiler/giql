package qiql

import (
	"fmt"
	"unicode"
)

type Scanner struct {
	Source string
	Tokens []Token

	Start   int
	Current int
}

func NewScanner(text string) Scanner {
	return Scanner{
		Source:  text,
		Start:   0,
		Current: 0,
	}
}

func (scanner *Scanner) ScanTokens() []Token {
	for !scanner.isAtEnd() {
		scanner.Start = scanner.Current
		scanner.scanToken()
	}
	scanner.addToken(NewToken(EOF, ""))

	return scanner.Tokens
}

func (scanner *Scanner) scanToken() {
	c := scanner.advance()
	switch c {
	case ':':
		scanner.addToken(NewToken(COLON, ":"))
	case '(':
		scanner.addToken(NewToken(LEFT_PAREN, "("))
	case ')':
		scanner.addToken(NewToken(RIGHT_PAREN, ")"))
	case '+':
		scanner.addToken(NewToken(PLUS, "+"))
	case '-':
		scanner.addToken(NewToken(MINUS, "-"))
	case '*':
		scanner.addToken(NewToken(STAR, "*"))
	case '~':
		scanner.addToken(NewToken(TILDE, "~"))
	case '#':
		scanner.addToken(NewToken(POUND, "#"))
	case '$':
		scanner.addToken(NewToken(DOLAR, "$"))
	case ' ':
	case '"':
		scanner.scanStrictLiteral()
	default:
		if unicode.IsDigit(c) {
			scanner.scanNumber()
		} else if isAlpha(c) {
			scanner.scanString()
		}
	}
}

func (scanner *Scanner) scanStrictLiteral() {
	for scanner.peek() != '"' && !scanner.isAtEnd() {
		scanner.advance()
	}

	if scanner.isAtEnd() {
		fmt.Println("ERROR: Unterminated string.")
		return
	}

	scanner.advance()

	value := scanner.substring(scanner.Start+1, scanner.Current-1)
	scanner.addToken(NewToken(STRICT, value))
}

func (scanner *Scanner) scanNumber() {
	for unicode.IsDigit(scanner.peek()) {
		scanner.advance()
	}

	if scanner.peek() == '.' && unicode.IsDigit(scanner.peekNext()) {
		scanner.advance()

		for unicode.IsDigit(scanner.peek()) {
			scanner.advance()
		}
	}

	value := scanner.substring(scanner.Start, scanner.Current)
	scanner.addToken(NewToken(NUMBER, value))
}

func (scanner *Scanner) scanString() {
	keywords := KeywordMap()

	for isAlphaNumeric(scanner.peek()) || isDot(scanner.peek()) {
		scanner.advance()
	}

	value := scanner.substring(scanner.Start, scanner.Current)

	if key, ok := keywords[value]; ok {
		scanner.addToken(NewToken(key, value))
	} else if scanner.peek() == ':' {
		scanner.addToken(NewToken(IDENTIFIER, value))
	} else {
		scanner.addToken(NewToken(STRING, value))
	}
}

func (scanner *Scanner) setPreviousTokenIdentifier() {
	scanner.Tokens[len(scanner.Tokens)-1].Type = IDENTIFIER
}

func (scanner *Scanner) advance() rune {
	char := scanner.charAt(scanner.Current)
	scanner.incremmentCurrent()

	return char
}

func (scanner *Scanner) match(expected rune) bool {
	if scanner.isAtEnd() {
		return false
	}
	if scanner.charAt(scanner.Current) != expected {
		return false
	}

	scanner.incremmentCurrent()
	return true
}

func (scanner *Scanner) isAtEnd() bool {
	return (scanner.Current >= len(scanner.Source))
}

func (scanner *Scanner) addToken(token Token) {
	scanner.Tokens = append(scanner.Tokens, token)
}

func (scanner *Scanner) substring(index_start, index_end int) string {
	return scanner.Source[index_start:index_end]
}

func (scanner *Scanner) charAt(index int) rune {
	var r rune = rune(scanner.Source[index])
	return r
}

func (scanner *Scanner) peek() rune {
	if scanner.isAtEnd() {
		return '\n'
	}
	return scanner.charAt(scanner.Current)
}

func (scanner *Scanner) peekNext() rune {
	if scanner.Current+1 >= len(scanner.Source) {
		return '\n'
	}
	return scanner.charAt(scanner.Current + 1)
}

func (scanner *Scanner) incremmentCurrent() {
	scanner.Current += 1
}

func isAlpha(c rune) bool {
	return unicode.IsLetter(c) ||
		c == '_'
}

func isDot(c rune) bool {
	return c == '.'
}

func isAlphaNumeric(c rune) bool {
	return unicode.IsDigit(c) || isAlpha(c)
}
