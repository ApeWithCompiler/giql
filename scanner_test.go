package qiql

import (
	"testing"
)

func TestScanTokens(t *testing.T) {
	sut := NewScanner("")

	want := 1
	got := sut.ScanTokens()

	if len(got) != want {
		t.Errorf("ScanTokens() returned %v tokens, want %v", len(got), want)
	}
}

func TestScanTokens_ParseInputString_GotLiteral(t *testing.T) {
	sut := NewScanner(`foo bar:baz"lorem ipsum"`)

	want := Token{Type: STRICT, Lexeme: "lorem ipsum"}
	got := sut.ScanTokens()

	if len(got) != 6 {
		t.Errorf("ScanTokens() returned %v tokens, want %v", len(got), 6)
	}

	if got[4] != want {
		t.Errorf("ScanTokens() returned wrong token %v, want %v", got[4], want)
	}
}

func TestScanTokens_ParseInt_GotNumber(t *testing.T) {
	sut := NewScanner("100")

	want := Token{Type: NUMBER, Lexeme: "100"}
	got := sut.ScanTokens()

	if len(got) != 2 {
		t.Errorf("ScanTokens() returned %v tokens, want %v", len(got), 2)
	}

	if got[0] != want {
		t.Errorf("ScanTokens() returned wrong token %v, want %v", got[0], want)
	}
}

func TestScanTokens_ParseDecimal_GotNumber(t *testing.T) {
	sut := NewScanner("13.37")

	want := Token{Type: NUMBER, Lexeme: "13.37"}
	got := sut.ScanTokens()

	if len(got) != 2 {
		t.Errorf("ScanTokens() returned %v tokens, want %v", len(got), 2)
	}

	if got[0] != want {
		t.Errorf("ScanTokens() returned wrong token %v, want %v", got[0], want)
	}
}

func TestScanTokens_ParseKeyword_GotAND(t *testing.T) {
	sut := NewScanner("lorem AND ipsum 13.37")

	want := Token{Type: AND, Lexeme: "AND"}
	got := sut.ScanTokens()

	if len(got) != 5 {
		t.Errorf("ScanTokens() returned %v tokens, want %v", len(got), 5)
	}

	if got[1] != want {
		t.Errorf("ScanTokens() returned wrong token %v, want %v", got[1], want)
	}
}

func TestScanTokens_ValidAssignement_GotIdentifier(t *testing.T) {
	sut := NewScanner("foo bar:baz")

	got := sut.ScanTokens()

	if len(got) != 5 {
		t.Errorf("ScanTokens() returned %v tokens, want %v", len(got), 5)
	}

	token_foo := Token{Type: STRING, Lexeme: "foo"}
	if got[0] != token_foo {
		t.Errorf("ScanTokens() returned wrong token %v, want %v", got[0], token_foo)
	}

	token_bar := Token{Type: IDENTIFIER, Lexeme: "bar"}
	if got[1] != token_bar {
		t.Errorf("ScanTokens() returned wrong token %v, want %v", got[1], token_bar)
	}

	token_colon := Token{Type: COLON, Lexeme: ":"}
	if got[2] != token_colon {
		t.Errorf("ScanTokens() returned wrong token %v, want %v", got[2], token_colon)
	}

	token_bazz := Token{Type: STRING, Lexeme: "baz"}
	if got[3] != token_bazz {
		t.Errorf("ScanTokens() returned wrong token %v, want %v", got[3], token_bazz)
	}
}

func TestScanTokens_ValidAssignementWithSpace_GotIdentifier(t *testing.T) {
	sut := NewScanner("foo bar: baz")

	got := sut.ScanTokens()

	if len(got) != 5 {
		t.Errorf("ScanTokens() returned %v tokens, want %v", len(got), 5)
	}

	token_foo := Token{Type: STRING, Lexeme: "foo"}
	if got[0] != token_foo {
		t.Errorf("ScanTokens() returned wrong token %v, want %v", got[0], token_foo)
	}

	token_bar := Token{Type: IDENTIFIER, Lexeme: "bar"}
	if got[1] != token_bar {
		t.Errorf("ScanTokens() returned wrong token %v, want %v", got[1], token_bar)
	}

	token_colon := Token{Type: COLON, Lexeme: ":"}
	if got[2] != token_colon {
		t.Errorf("ScanTokens() returned wrong token %v, want %v", got[2], token_colon)
	}

	token_bazz := Token{Type: STRING, Lexeme: "baz"}
	if got[3] != token_bazz {
		t.Errorf("ScanTokens() returned wrong token %v, want %v", got[3], token_bazz)
	}
}

func TestScanTokens_InvalidAssignement_GotString(t *testing.T) {
	sut := NewScanner("foo bar :baz")
	got := sut.ScanTokens()

	if len(got) != 5 {
		t.Errorf("ScanTokens() returned %v tokens, want %v", len(got), 5)
	}

	token_foo := Token{Type: STRING, Lexeme: "foo"}
	if got[0] != token_foo {
		t.Errorf("ScanTokens() returned wrong token %v, want %v", got[0], token_foo)
	}

	token_bar := Token{Type: STRING, Lexeme: "bar"}
	if got[1] != token_bar {
		t.Errorf("ScanTokens() returned wrong token %v, want %v", got[1], token_bar)
	}

	token_colon := Token{Type: COLON, Lexeme: ":"}
	if got[2] != token_colon {
		t.Errorf("ScanTokens() returned wrong token %v, want %v", got[2], token_colon)
	}

	token_bazz := Token{Type: STRING, Lexeme: "baz"}
	if got[3] != token_bazz {
		t.Errorf("ScanTokens() returned wrong token %v, want %v", got[3], token_bazz)
	}
}
