package qiql

import ()

type AstPrinterVisitor struct {
	Context string
}

func (v *AstPrinterVisitor) Print(expr Expr) string {
	v.Context = ""
	expr.Accept(v)
	v.appendString("\n")
	return v.Context
}

func (v *AstPrinterVisitor) VisitUnary(unary *Unary) {
	v.parenthesize(unary.Operator.Lexeme, unary.Right)
}

func (v *AstPrinterVisitor) VisitLiteral(literal *Literal) {
	v.appendString(literal.Value)
}

func (v *AstPrinterVisitor) VisitStrict(strict *Strict) {
	v.appendString(strict.Value)
}

func (v *AstPrinterVisitor) VisitGrouping(grouping *Grouping) {
	v.parenthesize("GROUP", grouping.Expr)
}

func (v *AstPrinterVisitor) VisitBoolean(boolean *Boolean) {
	v.parenthesize(boolean.Operator.Lexeme, boolean.Left, boolean.Right)
}

func (v *AstPrinterVisitor) VisitAssignment(assignment *Assignment) {
	v.parenthesize(":", assignment.Identifier, assignment.Value)
}

func (v *AstPrinterVisitor) parenthesize(name string, exprs ...Expr) {
	v.appendString("(")
	v.appendString(name)
	for _, e := range exprs {
		v.appendString(" ")
		e.Accept(v)
	}
	v.appendString(")")
}

func (v *AstPrinterVisitor) appendString(s string) {
	v.Context = v.Context + s
}
