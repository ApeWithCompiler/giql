# Grammar description
This file is supposed to give a human understandable overfiew of the grammar rules defined in grammar.txt

## Operators
| Example | Name | Description |
| ----------- | ----------- | ----------- |
| "x y" | STRING | exact search |
| x OR y | OR | match x or y |
| x AND y | AND | match x and y |
| NOT x | NOT | boolean not x |
| -x | MINUS | exclude x |
| +x | PLUS | exact match of x |
| ~x | TILDE | include synonyms |
| x * y | STAR | wildcard between x and y |
| (x OR y) z | LEFT_PAREN RIGHT_PAREN | control evaluation precedence |
| $ | DOLAR | search for currency |
| x: y | COLON | assign y to identifier x |
| #x | POUND | search for tag x |

## Expressions
| Name | Operators | Associates |
| ----------- | ----------- | ----------- |
| Unary | + - ~ # | Right |

## Assignments
Google has the concept of `filetype:`,`inurl:` or `intitle:` operators.
Giql treats them as "assginments". This is intended to provide the freedom of choosing which operators it will at use.
In the AST this results in an `asignment` having a `identifier` and `value` property.
