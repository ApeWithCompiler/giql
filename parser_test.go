package qiql

import (
	"testing"
)

func TestParser_ParseTokens(t *testing.T) {
	// Should render to string "x: y foo AND (bar OR ~baz)"
	// Using the scanner would be less insane,
	// break however the idom of a unit test.
	tokens := []Token{
		Token{
			Type:   IDENTIFIER,
			Lexeme: "x",
		},
		Token{
			Type:   COLON,
			Lexeme: ":",
		},
		Token{
			Type:   STRING,
			Lexeme: "y",
		},
		Token{
			Type:   STRICT,
			Lexeme: "foo",
		},
		Token{
			Type:   AND,
			Lexeme: "AND",
		},
		Token{
			Type:   LEFT_PAREN,
			Lexeme: "(",
		},
		Token{
			Type:   STRING,
			Lexeme: "bar",
		},
		Token{
			Type:   OR,
			Lexeme: "OR",
		},
		Token{
			Type:   TILDE,
			Lexeme: "~",
		},
		Token{
			Type:   STRING,
			Lexeme: "baz",
		},
		Token{
			Type:   RIGHT_PAREN,
			Lexeme: ")",
		},
		Token{
			Type:   EOF,
			Lexeme: "EOF",
		},
	}
	sut := NewParser(tokens)
	got := sut.Parse()

	printer := TypeVisitor{}

	if printer.Print(got[0]) != "assignment" {
		t.Errorf("Parse() returned %v type, want %v", got[0], "assignment")
	}

	if printer.Print(got[1]) != "boolean" {
		t.Errorf("Parse() returned %v type, want %v", got[1], "boolean")
	}
}

func TestParser_DetectsInfiniteLoop(t *testing.T) {
	scanner := NewScanner(`foo AND bar OR baz)`)
	tokens := scanner.ScanTokens()

	sut := NewParser(tokens)
	sut.Parse()

	if sut.HasErrors() != true {
		t.Errorf("Parse() failed to recognize infinite loop!")
	}

	err := sut.Errors[1]
	want := "Infinite loop detected."
	if err.Message != want {
		t.Errorf("Parse() infinite loop returned wrong error: %v", err.Message)
	}
}

func TestParser_DetectsInvalidAssignement(t *testing.T) {
	scanner := NewScanner(`foo:`)
	tokens := scanner.ScanTokens()

	sut := NewParser(tokens)
	sut.Parse()

	if sut.HasErrors() != true {
		t.Errorf("Parse() failed to recognize invalid assignement!")
	}

	err := sut.Errors[0]
	want := "Expect Value for assignement."
	if err.Message != want {
		t.Errorf("Parse() invalid assignement returned wrong error: %v", err.Message)
	}
}
