package qiql

import ()

type Visitor interface {
	VisitUnary(unary *Unary)
	VisitLiteral(literal *Literal)
	VisitStrict(strict *Strict)
	VisitBoolean(boolean *Boolean)
	VisitGrouping(grouping *Grouping)
	VisitAssignment(assignment *Assignment)
}
