# Google Inspired Query Language

This module contains a parser for a string, similar to googles search input, into a indermediate format.
In a naive approach this would be as trivial as just returning this string.
This would then be missing out on advanced search operators and boolean search logic.

Ignoring the advanced nlp features, aswell as user data, this can be used as a approximation to googles search capabilities, at best.

## Usage
### Query grammar

For information how a query is parsed, take a look at the [grammar description](grammar_description.md).

### Integration
![integration image](docs/giql_integration.png)

As demonstrated in the above image, giql only concernes around translating the **query** into an **AST**.
The **Reparser** in this context is the implementation of translating this **AST** into the  storage/index technologie used
in the backend.

The benefit is to uniformly handle the user query based on some principles of compiler theory.

### Example
```go
package main

import (
    giql "gitlab.com/ApeWithCompiler/giql"
)

func doParse(query string) []giql.Expr {
    // Initialize the "scanner"
    // This will process the characters into
    // tokens.
    scanner := giql.NewScanner(query)
    tokens := scanner.ScanTokens()

    // The parser will parse these tokens to an
    // AST.
    // However, as you can put an undefined number
    // of words into the query, it will return a list of
    // ASTs. As for every syntactic expression it can find, one.
    parser := giql.NewParser(tokens)
    exprs := parser.Parse()

    // If the parser occured any error you can check so.
    // the .Errors property can tell you more.
    // Also you can use the "FallbackParser".
    // This will simply extract all STRING Tokens,
    // without advanced syntax
    if parser.HasErrors() {

    }
    return exprs
}
```

## Testing
For basic verification of some functionallity, unit tests are included. This includes scanning a string to tokens, aswell as parsing tokens to an ast.

Run as following

```bash
cd ./giql
go test
```

This should run trough the tests and report if there is a error found.

## Contributing
I need to confess to be neither a professional dev, nor a student of any matter.
So you will propably find limitless oppurtunities to improve this.
If you do so, I would be happy to learn, or at least drive this project forward for others to be usable.

Some direct effort I am very happy to see, is a sane way of handling errors in the input. Mostly on the parsers side.
